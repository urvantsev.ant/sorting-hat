import discord
import os
import random

# create intents before creating bot/client instance
intents = discord.Intents().default()
intents.members = True

client = discord.Client(intents=intents)

channels = {
    'classroom-1': 892089864722477127,
    'classroom-2': 892090380705759252,
    'classroom-3': 892090414415351850,
    'classroom-4': 892090454710030397
}


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('$join'):
        member = message.author
        available_channels = channels.copy()

        if member.voice.channel.name in available_channels:
            del available_channels[member.voice.channel.name]

        for key in channels:
            if key in available_channels:
                channel = client.get_channel(available_channels[key])

                if len(channel.members) == channel.user_limit:
                    del available_channels[key]

        channel = client.get_channel(
            random.choice(list(available_channels.values())))

        await member.move_to(channel)

client.run(os.getenv('TOKEN'))
